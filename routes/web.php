<?php

Route::get('/', 'FootballController@index');
Route::get('/group/{group}', 'FootballController@show');
Route::post('/group/{group}','FootballController@createList');
//Route::post('/createList','FootballController@createList');

Route::post('/store','FootballController@store');

Route::post('/destroy','FootballController@destroy');
Route::post('/create','FootballController@create');


