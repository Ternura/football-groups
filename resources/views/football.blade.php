<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
{{--    <meta name="csrf-token" content="{{ csrf_token() }}" />--}}

    <title>Football Group</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css"  />

</head>
<body>
    <div class="flex-center position-ref content m-b-md">
        <form id="myform" method="get" action="/store" autocomplete="off" novalidate>
            @csrf

        <div id="createNewGroup">
            Group:
            <button type="button" class="createNew">New</button>
            <ul id="newGroup">

                @if (count($groups) > -1)
                    @foreach($groups as $group)
                        <li class="active{{$group->group}}">
                            <a href="/group/{{$group->group}}">{{$group->group}}</a>
                            <span class="current{{$group->group}} spanFootballBlade"> X </span>
                            <input value={{$group->group}} name={{$group->group}}>
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>
        </form>
    </div>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/jQuery3.4.1.js') }}"></script>
</body>
</html>
