<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<meta name="_token" content="{!! csrf_token() !!}">--}}
    <title>Group {{$group}}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css"  />

</head>
<body>
<div class="flex-center position-ref ">
    <div class="content1">
        <div class="title m-b-md">
            {{--top--}}
            <a class="linkBack" href="/"><!--
            --><span class="goBack arrow">&#8592</span><!--
            --><span class="goBack">GO BACK</span></a>
           {{--end of top--}}
            <div class="mainContent">

                    <span class="group">Group:</span>
                    <span class="group">{{$group}}</span>
                    <br>
                    <span class="group">Team:</span>
                    <input class="inputTeam" type="text"  value="" autocomplete="off">
                    <button type="button" id="add" class="generate" >Add</button>
                    <br>
                <div>
                    <span class="team">Teams:</span>
                </div>
                <section class="main-section">
                <div  id="deleteTeam">
                    <ul class="delete-x">

                            {{--@foreach($teams as $team)--}}
                                {{----}}
                                {{--<li>--}}

                                    {{--<span class={{$teams->$team}}> X </span>--}}
                                {{--</li>--}}
                            {{--@endforeach--}}

                    </ul>
                </div>
                    <ol class="team-list">

                    </ol>
                </section>
                    <div>
                    <span class="matches">Matches:</span>
             <button id="generate" type="button" class="generate">Generate</button>
                    </div>
                <br>
                <div class="rezultMatches"></div>

            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>
{{--https://code.jquery.com/jquery-3.3.1.min.js--}}
<script src="{{ asset('js/jQuery3.4.1.js') }}"></script>
</body>
</html>
