<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsResultTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->char('group');
            $table->string('team');
            $table->decimal('result');
            $table->decimal('hide_key');
            $table->boolean('is_val');

//            $table->foreign('is_val')->references('is_value')->on('group');
            $table->foreign('group')->references('group')->on('group');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result');
    }
}
