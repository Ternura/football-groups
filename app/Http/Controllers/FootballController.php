<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;



class FootballController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = DB::table('group')->get();
//        $results = DB::select('select * from users where id = ?', [1]);
//        $groups = DB::select('select * from group');
        return  view('football', ['groups' => $groups]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $team, $result = 0, $hide_key = 0, $is_value = true)
    {
        $team = $request->input('value');//team in table
        $group = $request->input('group');
        $result = $request->input('result');
        $hide_key = $request->input('hide_key');

        DB::transaction(function() use ($group, $team, $result,  $hide_key, $is_value)
        {
            DB::table('group')->where('group', $group)->update(['is_value' => $is_value]);

            DB::table('result')->insert(
                ['group' => $group,
                 'team' =>  $team,
                    'result' => 0,
                    'hide_key' => $hide_key,
                    'is_val' => $is_value]
            );
        });
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $is_value = false)
    {
        $value = $request->input('value');

        DB::table('group')->insert(
            ['group' => $value,
                'is_value' => $is_value]
        );
        //дані додані в БД!
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($group)
    {
         $group;
        if(isset($_SERVER['REQUEST_URI'])) {
            $group = $_SERVER['REQUEST_URI'];
            $group = explode("/", $group);
            $irrelevantEl = $group[2]; //дістали назву групи із url

           if ((count($group) > 3) ) {//redirect on 404.php
              return view(redirect('404.php'));
           }
           else {
               return view('group', array('group' => $irrelevantEl));
           }
        }
    }
public function createList() {
    $group = $_SERVER['REQUEST_URI'];
    $group = explode("/", $group);
    $irrelevantEl = $group[2]; //дістали назву групи із url
    $teams = DB::table('result')
        ->select('*')
        ->where('group', '=', $irrelevantEl)
        ->get();
    return view('group', array('teams'=> $teams));
//               $teams = DB::select('select * from result where group = :group', ['group' => $irrelevantEl]);
}
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $is_value = false)
    {
        $value = $request->input('value');

        DB::table('group')->where('group', '=', $value)->delete();
    }
}
